
/*@#common-include#(three.js)*/
/*@#common-include#(Constants.js)*/
/*@#common-include#(Helpers.js)*/
/*@#common-include#(IntersectionFinder.js)*/
/*@#common-include#(HelperLinesHandler.js)*/
/*@#common-include#(ScreenMovementHandler.js)*/
/*@#common-include#(Display2D.js)*/
/*@#common-include#(ElementCreation.js)*/

var api = {
	initQuestion: function(cst, st) {
		const wrapper = document.getElementById(questionLocalID + "_wrap");
		const dimensionProvider = () => Object.assign({ width: cst.canvasWidth || 500, height: cst.canvasHeight || 500 });
		const wrapperDimensions = dimensionProvider();
		const renderer = getRenderer(wrapperDimensions, BACKGROUND_COLOR);
		const scene = new THREE.Scene();
		const camera = default2DCamera(PIXELS_PER_UNIT, wrapperDimensions);
		const grid = construct2DGrid(cst.maxAxis, CENTER_LINE_COLOR, GRID_COLOR, DEFAULT_Z_2D - 10);
		const snapToGrid = true;

		wrapper.appendChild(renderer.domElement);
		scene.add(grid);

		const movementHandler = new ScreenMovementHandler(wrapper, renderer, camera, RIGHT_MOUSE_BUTTON);
		wrapper.addEventListener('mousedown', movementHandler.onMousedown);
		wrapper.addEventListener('mouseup', movementHandler.onMouseup);
		wrapper.addEventListener('mousemove', movementHandler.onMousemove);
		wrapper.addEventListener('wheel', movementHandler.onMousewheel);
		wrapper.oncontextmenu = e => e.preventDefault();
		wrapper.onwheel = e => e.preventDefault();

		const intersectionFinder = new IntersectionFinder(camera, 
			() => renderer.context.canvas.getBoundingClientRect(), new THREE.Raycaster());

		const helperLinesHandler = new HelperLinesHandler(snapToGrid, grid, intersectionFinder, wrapper, 
			scene, cst.maxAxis);
		wrapper.addEventListener('mousemove', helperLinesHandler.onMousemove);
		wrapper.appendChild(helperLinesHandler.positionDataElement);

		window.addEventListener('resize', 
			onWindowResize(PIXELS_PER_UNIT, dimensionProvider, intersectionFinder, camera, renderer), 
			false);

		function animate() {
				requestAnimationFrame(animate);
				renderer.render(scene, camera);
		}

		animate();

		function houseWidthHeight(material, width, height) {
				const w = width;
				const h = height;
				const shiftWindowDoor = w / 4;

				const styledTriangle = (A, B, C) => triangle(A, B, C, material);
				const styledSquare = (A, dim) => square(A, dim, material);

				const dim = (w, h) => Object.assign({ width: w, height: h });
				const base = styledSquare(vec2(0, w / 2), dim(w, w));
				const door = styledSquare(vec2(shiftWindowDoor, shiftWindowDoor), dim(w / 4, w / 2));
				const window = styledSquare(vec2(-shiftWindowDoor, 0.75 * w), dim(w / 4, w / 4));
				const top = styledTriangle(vec2(-w / 2, w), vec2(w / 2, w), vec2(0, w + (h - w)));

				const composite = new THREE.Object3D();
				composite.add(base);
				composite.add(top);
				composite.add(door);
				composite.add(window);

				return composite;
		}

		const house = material => houseWidthHeight(material, cst.houseWidth, cst.houseHeight);

		const originalHouse = house({ color: 0x333333, linewidth: 2 });
		const transformedHouse = house({ color: LINE_COLOR_UNSELECTED, linewidth: 2 });

		scene.add(applyTransformToChildren(originalHouse, cst.opsOnFirstHouse));
		scene.add(applyTransformToChildren(transformedHouse, cst.opsOnSecondHouse));

		const table = document.getElementById(questionLocalID + "_table");
		const tbody = table.getElementsByTagName("tbody")[0];

		class UserInputs {
			constructor(x, y, angle) {
				this.x = x; this.y = y; this.angle = angle;
			}
			asList() {
				return [ this.x, this.y, this.angle ];
			}
		}

		class UserInputRow {
			constructor(rowTypeInput, userInputs, removeButton) {
				this.rowTypeInput = rowTypeInput; 
				this.userInputs = userInputs; 
				this.removeButton = removeButton;
				rowTypeInput.onchange = () => {
					const selection = rowTypeInput.selectedOptions[0];

					userInputs.x.disabled = userInputs.y.disabled = selection.value == ROTATION;
					userInputs.angle.disabled = selection.value != ROTATION;

					if (selection.value == ROTATION) {
						userInputs.x.value = userInputs.y.value = "";
					} else {
						userInputs.angle.value = "";
					}
				};

				rowTypeInput.onchange();
			}

			asList() {
				return [this.rowTypeInput]
					.concat(this.userInputs.asList())
					.concat([this.removeButton]);
			}

			set(userInputs) {
				this.rowTypeInput.value = userInputs.name;
				this.userInputs.x.value = userInputs.x;
				this.userInputs.y.value = userInputs.y;
				this.userInputs.angle.value = userInputs.angle;

				this.rowTypeInput.onchange();
			}
		}

		function generateUserInputs() {
			const choice = createChoice([MOVE, ROTATION, SCALE]);
			const ins = [ createInput(), createInput(), createInput() ];
			const rem = createRemoveBtn();
			return new UserInputRow(choice, new UserInputs(ins[0], ins[1], ins[2]), rem);
		}

		const rows = [];
		const userInputs = [];
		const addButton = document.getElementById(questionLocalID + "_add_transform");

		function generateTableRowForUserInputs(userInputsRow) {
			const row = ce("tr");

			userInputsRow.removeButton.addEventListener('click', e => {
				tbody.removeChild(row);

				const i = rows.indexOf(row);
				rows.splice(i, 1);
				userInputs.splice(i, 1);
			});

			userInputsRow.asList().map(ui => wrapTd(ui)).forEach(el => row.appendChild(el));
			return row;
		}

		function addNewInput() {
			const newInputs = generateUserInputs();
			userInputs.push(newInputs);

			const row = generateTableRowForUserInputs(newInputs)
			rows.push(row);
			tbody.appendChild(row);
		}

		addButton.addEventListener('click', addNewInput);

		this.takeState = () => userInputs.map(userInputRow => {
			const userInput = userInputRow.userInputs;
			const name = userInputRow.rowTypeInput.value;
			const defaultVal = name == SCALE ? 1 : 0;
			return { 
				name: name,
				x: userInput.x.value || defaultVal, 
				y: userInput.y.value || defaultVal, 
				angle: userInput.angle.value || defaultVal
			};
		});
					
		const setValueDict = {
			"INPUT": (elementName, val) => elementName.value = val,
			"SELECT": (elementName, val) => { 
				Object.values(elementName.children)
					.filter(ch => ch.value == val)
					.forEach(ch => ch.selected = true);
				elementName.onchange();
			},
			"BUTTON": () => {}
		};

		function removeAllInputs() {
			while (rows.length) {
				tbody.removeChild(rows.pop());
				userInputs.pop();
			}
		}

		this.resetState = (cst, st) => {
			removeAllInputs();
			userInputs.forEach(userInput => 
					userInput.asList().forEach(domEl => 
						setValueDict[domEl.tagName](domEl, "") ));
		};

		this.revertState = (cst, st) => {
			removeAllInputs();
			st.qs.forEach(addNewInput);
			userInputs.forEach((input, i) => input.set(st.qs[i]));
		};

		if (st.qs && st.qs.length && st.qs[0]) {
			this.revertState(cst, st);
		}

		if (st.disabled) {
			addButton.disabled = true;

			rows.forEach(row => {
				for (var el of row.children)
					el.children[0].disabled = true;
			});

			if (st.userSubmittedOps) {
				const houseWithUserTransformsApplied = house({ color: 0x00ff00 });
				applyTransformToChildren(houseWithUserTransformsApplied, st.userSubmittedOps);
				scene.add(houseWithUserTransformsApplied);
			}
		}

	}
};

return api;

