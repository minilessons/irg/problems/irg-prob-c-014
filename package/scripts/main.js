/*@#common-include#(three.js)*/
/*@#common-include#(Constants.js)*/
/*@#common-include#(Helpers.js)*/
/*@#include#(ServerSideHelpers.js)*/

const toMatrix = {};
toMatrix[MOVE] = function(object) { return mat4().makeTranslation(object.x, object.y, 0); };
toMatrix[SCALE] = function(object) { return mat4().makeScale(object.x, object.y, 0); };
toMatrix[ROTATION] = function(object) { return mat4().makeRotationZ(object.angle * (Math.PI / 180)); };

const reverses = {};
reverses[MOVE] = function(object) { 
	const newObj = copy(object); 
	newObj.x = -newObj.x; newObj.y = -newObj.y;
	return newObj; 
};

reverses[SCALE] = function(object) { 
	const newObj = copy(object);
	newObj.x = 1 / newObj.x; newObj.y = 1 / newObj.y; 
	return newObj;
};

reverses[ROTATION] = function(object) { 
	const newObj = copy(object);
	newObj.angle = -newObj.angle;
	return newObj;
};

function randomRotation(allowedAngles) {
	const index = random(allowedAngles.length - 1);
	const angle = allowedAngles[index];
	const angleInDegrees = Math.round((angle / Math.PI) * 180);
	return {
		description: { name: ROTATION, angle: angleInDegrees },
		matrix: mat4().makeRotationZ(angle)
	};
}

function randomTranslation(limit) {
	const pt = randomPoint(limit);
	return {
		description: { name: MOVE, x: pt.x, y: pt.y },
		matrix: mat4().makeTranslation(pt.x, pt.y, 0)
	};
}

function randomScale(limit) {
	const v1 = randomSign() * (random(limit - 1) + 1);
	const v2 = randomSign() * (random(limit - 1) + 1);
	return {
		description: { name: SCALE, x: v1, y: v2 },
		matrix: mat4().makeScale(v1, v2, 1)
	};
}

function randomTransformations(allowedAngles, translateLimit, scaleLimit) {
	const ops = [
		function() { return randomScale(scaleLimit); },
		function() { return randomRotation(allowedAngles); },
		function() { return randomTranslation(translateLimit); }
	];

	var matrix = mat4();
	const transforms = [];

	for (var i = 0; i < ops.length; i++) {
		const newTransform = ops[i]();
		const transformDescription = newTransform.description;
		const transformMatrix = newTransform.matrix;

		transforms.push(transformDescription);
		matrix = transformMatrix.multiply(matrix);
	}

	return { transformMatrix: matrix, 
					 transformDescriptions: transforms };
}

function reverseTransformations(listOfTransformations) {
	var matrix = mat4();
	const transforms = [];
	
	for (var i = listOfTransformations.length - 1; i >= 0; i--) {
		const curr = listOfTransformations[i];
		const name = curr.name;

		const reverseDescription = reverses[name](curr);
		const newTransform = toMatrix[name](reverseDescription);

		transforms.push(reverseDescription);
		matrix = newTransform.multiply(matrix);
	}

	return { transformMatrix: matrix,
					 transformDescriptions: transforms };
}

// Ova je metoda namijenjena inicijalizaciji zadatka.
// Dobiva questionConfigData koji postavlja editor konfiguracije
// -------------------------------------------------------------------------------------------------
function questionInitialize(questionConfig) {
	const allowedAngles = questionConfig.allowedAngles.map(function(multiplier) { return Math.PI * multiplier; });
	const scaleLimit = questionConfig.scaleLimit;
	const translateLimitOnFirst = questionConfig.translateLimitForFirstHouse;
	const translateLimitOnSecond = questionConfig.translateLimitForSecondHouse;
	const houseWidth = questionConfig.houseWidth;
	const houseHeight = questionConfig.houseHeight;
	const opsOnFirstHouse = randomTransformations(allowedAngles, translateLimitOnFirst, scaleLimit);
	const opsOnFirstHouseReverse = reverseTransformations(opsOnFirstHouse.transformDescriptions);
	const opsOnSecondHouse = randomTransformations(allowedAngles, translateLimitOnSecond, scaleLimit);
	const maxAxis = translateLimitOnSecond + Math.max(houseWidth, houseHeight) * scaleLimit;

	userData.question = { 
		opsOnFirstHouse: opsOnFirstHouse.transformMatrix.clone(), 
		opsOnSecondHouse: opsOnSecondHouse.transformMatrix.clone(),
		maxAxis: maxAxis,
		houseWidth: houseWidth,
		houseHeight: houseHeight,
		canvasWidth: questionConfig.canvasWidth,
		canvasHeight: questionConfig.canvasHeight
	};

	userData.question.correct = { 
		// .multiply modificira in-place
		ops: opsOnSecondHouse.transformMatrix.clone().multiply(opsOnFirstHouseReverse.transformMatrix)
	};

	userData.correctQuestionState = opsOnFirstHouseReverse.transformDescriptions
		.concat(opsOnSecondHouse.transformDescriptions);
	userData.questionState = [];
	userData.userSubmittedOps = mat4();
}

// Ova je metoda namijenjena generiranju varijabli koje se dinamički računaju i nigdje ne pohranjuju.
// Ove varijable bit će vidljive kodu koji HTML-predloške odnosno ServerSide-JavaScript-predloške
// lokalizira.
// -------------------------------------------------------------------------------------------------
function getComputedProperties() {
  return {};
}

// Ova metoda poziva se kako bi se obavilo vrednovanje zadatka. Pozivatelju treba vratiti objekt:
// {correctness: X, solved: Y}, gdje su X in [0,1], Y in {true,false}.
// -------------------------------------------------------------------------------------------------
function questionEvaluate() {
	const EPSILON = 0.001;

	function constructUserMatrix(qs) {
		var matrix = mat4();

		for (var i = 0; i < qs.length; i++) {
			const row = qs[i];
			const name = row.name;
			const currentMatrix = toMatrix[name](row);

			matrix = currentMatrix.multiply(matrix);
		}

		return matrix;
	}

	function dot(els, v1, v2, v3, v4) {
		const m1 = mat4();
		m1.elements = els;

		const m2 = mat4();

		// column-major
		m2.elements = [ v1.x, v2.x, v3.x, v4.x, 
										v1.y, v2.y, v3.y, v4.y, 
											 0,	   0, 	 0, 	 0, 
									  	 1,		 1, 	 1, 	 1 ];

		return m1.multiply(m2.transpose());
	}

	function checkEq(sol1, sol2) {
		for (var i = 0; i < sol1.elements.length; i++) {
			const equalPts = deq(sol1.elements[i], sol2.elements[i], EPSILON);
			if (!equalPts) return 0;
		}

		return 1;
	}

	const p1 = vec2(0, 0);
	const p2 = vec2(0, 3);
	const p3 = vec2(3, 0);
	const p4 = vec2(3, 3);

	const user = constructUserMatrix(userData.questionState);
	const solution = userData.question.correct.ops;

	const userSol = dot(user.clone().elements, p1, p2, p3, p4);
	const corrSol = dot(solution.elements, p1, p2, p3, p4);

	userData.userSubmittedOps = user.clone();
	userData.question.userTransformedPoints = userSol.elements;
	userData.question.correctTransformedPoints = corrSol.elements;
	userData.isSubmitted = true;

	return { correctness: checkEq(userSol, corrSol),
			 		 solved: userData.questionState.length > 0 };
}

// Metoda koja se poziva kako bi izvadila zajednički dio stanja zadatka koje je potrebno za prikaz zadatka.
// -------------------------------------------------------------------------------------------------
function exportCommonState() {
	const common = copy(userData.question);
	common.correct = undefined;

	return common;
}

// Metoda koja se poziva kako bi izvadilo stanje zadatka koje je postavio korisnik (njegovo rješenje).
// Razlikuje se svrha prikaza: korisnikovo rješenje ili točno rješenje
// -------------------------------------------------------------------------------------------------
function exportUserState(stateType) {
  if (stateType == "USER") {
    return { qs: userData.questionState, 
			 			 userSubmittedOps: userData.userSubmittedOps.clone()
								.multiply(userData.question.opsOnFirstHouse), 
			 			 disabled: userData.isSubmitted };
  }

  if (stateType == "CORRECT") {
    return { qs: userData.correctQuestionState, 
						 solutionOps: userData.question.solutionOps,
						 disabled: true };
  }

  return {};
}

// Metoda koja se poziva kako bi vratila stanje zadatka koje odgovara nerješavanom zadatku.
// -------------------------------------------------------------------------------------------------
function exportEmptyState() {
  return { };
}

// Metoda koja se poziva kako bi pohranila stanje zadatka na temelju onoga što je korisnik napravio.
// -------------------------------------------------------------------------------------------------
function importQuestionState(data) {
  userData.questionState = data;
	userData.userSubmittedOps = mat4();
}
