
function _getCursorPosition(clientX, clientY, containerDimensions) {
        var x = clientX - containerDimensions.left;
        var y = clientY - containerDimensions.top;
        return {x: x, y: y};
}

function toNormalizedWindowCoordinates(x, y, containerDimensions) {
    const realPos = _getCursorPosition(x, y, containerDimensions);
    return new THREE.Vector2( (realPos.x / containerDimensions.width) * 2 - 1, 
                             -(realPos.y / containerDimensions.height) * 2 + 1);
}

class IntersectionFinder {

    constructor(camera, containerDimensionsSupplier, raycaster) {
        this.camera = camera;
        this.containerDimensionsSupplier = containerDimensionsSupplier;
        this.raycaster = raycaster;

        this.getIntersections = this.getIntersections.bind(this);
    }

    getIntersections(clientX, clientY, objects) {
        const pos = toNormalizedWindowCoordinates(clientX, clientY, this.containerDimensionsSupplier());
        this.raycaster.setFromCamera(pos, this.camera);

        return this.raycaster.intersectObjects(objects, true);
    }

}
