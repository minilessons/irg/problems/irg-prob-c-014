
function pushAsVec3(geometry, vec2) {
    geometry.vertices.push(toVec3(vec2));
}

function line(A, B, materialProperties) {
    const geometry = new THREE.Geometry();
    const material = new THREE.LineBasicMaterial(materialProperties);

    pushAsVec3(geometry, A);
    pushAsVec3(geometry, B);

    return new THREE.LineSegments(geometry, material);
}

function getWireframe(geometry, materialProperties) {        
    const geom = new THREE.EdgesGeometry(geometry);
    const mat = new THREE.LineBasicMaterial(materialProperties);

    return new THREE.LineSegments(geom, mat);
}

function triangle(A, B, C, materialProperties) {
    const geometry = new THREE.Geometry();

    pushAsVec3(geometry, A);
    pushAsVec3(geometry, B);
    pushAsVec3(geometry, C);
    geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );

    return getWireframe(geometry, materialProperties);
}

function quadrilateral(A, B, C, D, materialProperties) {
    const geometry = new THREE.Geometry();

    pushAsVec3(geometry, A);
    pushAsVec3(geometry, B);
    pushAsVec3(geometry, C);
    pushAsVec3(geometry, D);

    geometry.faces.push(new THREE.Face3(0, 1, 2));
    geometry.faces.push(new THREE.Face3(0, 3, 2));

    return getWireframe(geometry, materialProperties);
}

function square(loc, dim, materialProperties) {
    const material = new THREE.MeshBasicMaterial({ color: 0x000000 });
    const geometry = new THREE.BoxGeometry(dim.width, dim.height, DEFAULT_Z_2D);
    
    geometry.translate(loc.x, loc.y, 0);
    geometry.computeFaceNormals();

    return getWireframe(geometry, materialProperties);
}

function filledTriangle(A, B, C) {
    var geometry = new THREE.Geometry();

    pushAsVec3(geometry, A);
    pushAsVec3(geometry, B);
    pushAsVec3(geometry, C);

    geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );
    geometry.computeFaceNormals();

    return new THREE.Mesh(geometry, new THREE.MeshNormalMaterial() );
}

