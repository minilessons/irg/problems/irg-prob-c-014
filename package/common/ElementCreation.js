
const ce = (e) => document.createElement(e);

function wrapTd(el) {
	const td = ce("td");
	td.appendChild(el);
	return td;
}

function createNameElement(pointName) {
	const div = ce("div");
	div.setAttribute("class", "form-control");
	div.setAttribute("disabled", true);
	div.innerHTML = pointName;
	return div;
}

function createInput(inputName) {
	const input = ce("input");
	input.setAttribute("type", "number");
	input.setAttribute("class", "form-control");
	return input;
}

function createChoice(options) {
	const createOption = (display, value) => {
		const o = ce("option");
		o.innerHTML = display;
		o.setAttribute("value", value);
		return o;
	}

    const optionEls = options.map(o => createOption(o, o));
	const sel = ce("select");

	sel.setAttribute("name", "selectbasic"); 
	sel.setAttribute("class", "form-control");
	sel.setAttribute("style", "width: 100%;");
	optionEls.forEach(el => sel.appendChild(el));

	return sel;
}

function createRemoveBtn() {
    const btn = ce("button");
    btn.setAttribute("class", "btn btn-danger");
    btn.innerHTML = "-";
    
    return btn;
}
